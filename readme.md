## Teamliquid Dark Theme

Frontend Enhancement project for a popular e-sport community site.

## Install

Available for Google Chrome on [Chrome Store](https://chrome.google.com/webstore/detail/teamliquid-dark-theme/amhleonjhpnnjbciomimcenodbaefkpl) as an extension.

### Overview - Before:

![screenshot](http://puu.sh/4sKM1/5c61d56852.jpg)

### Overview - After:

![screenshot](http://puu.sh/4sKYm/08164a7d9f.jpg)

## Features
 
- Dark style
- Quick Quote
- Streamer Shortcuts
- Button Accesskeys
- and more!

Powered by jQuery, Adobe Fireworks ([R.I.P](http://blogs.adobe.com/fireworks/2013/05/the-future-of-adobe-fireworks.html)) and ofcourse the original [Teamliquid community website](http://www.teamliquid.net/).

## Feedback, Contributions & Questions

All are very welcome. You can contact me on <eaglesight@comhem.se> or simply start forking!
To test the extension locally in Chrome, simply open [chrome://extensions/](extensions), click the 'fetch uncompressed extension' button and locate the TeamLiquid-Dark-Theme/.extensions/chrome folder.