// main.js - Teamliquid Dark Theme's module
// author: eaglesight

var data = require("self").data;
var assets = data.url("assets/");
var images = data.url("assets/images/");

// include script.
require("page-mod").PageMod({
  include: ["*.teamliquid.net"],
  
  // userscript, userstyle
  contentScriptWhen: 'end',
  contentScriptFile: [data.url("assets/jquery-2.0.3.min.js"), data.url("assets/main.js")],
  contentStyleFile: data.url("assets/main.css"),
  onAttach: function(worker) {
    worker.postMessage(assets);
  }
});

// Construct a panel, loading its content from the "text-entry.html"
// file in the "data" directory, and loading the "get-text.js" script
// into it.
var toggle_options = require("panel").Panel({
  width: 652,
  height: 440,
  contentURL: data.url("popup.html"),
  contentScriptFile: data.url("popup.js")
});
 
// Create a widget, and attach the panel to it, so the panel is
// shown when the user clicks the widget.
require("widget").Widget({
  label: "Make Dark Theme act the way you want it to!",
  id: "toggle-options",
  contentURL: data.url("icon_16.png"),
  panel: toggle_options
});
