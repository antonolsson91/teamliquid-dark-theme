// Called when the url of a tab changes.
function checkForValidUrl(tabId, changeInfo, tab) {
    if(changeInfo.status === "loading") {
        if (tab.url.indexOf('teamliquid.net') > -1) {
            chrome.pageAction.show(tabId);
        }
    }
}
// Listen for any changes to the URL of any tab.
chrome.tabs.onUpdated.addListener(checkForValidUrl);

chrome.tabs.onSelectionChanged.addListener(function(tabId, selectInfo){
    if(true) {
        checkForValidUrl(tab);
    }
});

var optionStorage = localStorage.getItem('tl_darktheme-options');
console.log(optionStorage);

chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
	console.log(sender.tab ? "from a content script:" + sender.tab.url : "from the extension");	
	if (request.message == "giveOptionStorage"){
		sendResponse({optionStorage: optionStorage});
	}
  });

  chrome.extension.onConnect.addListener(function(port) {
  console.assert(port.name == "tldt");
  port.onMessage.addListener(function(msg) {
    optionStorage = msg.optionStorage;
	console.log("msg.optionStorage: " + msg.optionStorage);
	console.log("optionStorage: " + optionStorage);
  });
});