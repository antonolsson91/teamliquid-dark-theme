﻿/*! sprintf.js | Copyright (c) 2007-2013 Alexandru Marasteanu <hello at alexei dot ro> | 3 clause BSD license | Info: http://www.diveintojavascript.com/projects/javascript-sprintf */
(function(e){function r(e){return Object.prototype.toString.call(e).slice(8,-1).toLowerCase()}function i(e,t){for(var n=[];t>0;n[--t]=e);return n.join("")}var t=function(){return t.cache.hasOwnProperty(arguments[0])||(t.cache[arguments[0]]=t.parse(arguments[0])),t.format.call(null,t.cache[arguments[0]],arguments)};t.format=function(e,n){var s=1,o=e.length,u="",a,f=[],l,c,h,p,d,v;for(l=0;l<o;l++){u=r(e[l]);if(u==="string")f.push(e[l]);else if(u==="array"){h=e[l];if(h[2]){a=n[s];for(c=0;c<h[2].length;c++){if(!a.hasOwnProperty(h[2][c]))throw t('[sprintf] property "%s" does not exist',h[2][c]);a=a[h[2][c]]}}else h[1]?a=n[h[1]]:a=n[s++];if(/[^s]/.test(h[8])&&r(a)!="number")throw t("[sprintf] expecting number but found %s",r(a));switch(h[8]){case"b":a=a.toString(2);break;case"c":a=String.fromCharCode(a);break;case"d":a=parseInt(a,10);break;case"e":a=h[7]?a.toExponential(h[7]):a.toExponential();break;case"f":a=h[7]?parseFloat(a).toFixed(h[7]):parseFloat(a);break;case"o":a=a.toString(8);break;case"s":a=(a=String(a))&&h[7]?a.substring(0,h[7]):a;break;case"u":a>>>=0;break;case"x":a=a.toString(16);break;case"X":a=a.toString(16).toUpperCase()}a=/[def]/.test(h[8])&&h[3]&&a>=0?"+"+a:a,d=h[4]?h[4]=="0"?"0":h[4].charAt(1):" ",v=h[6]-String(a).length,p=h[6]?i(d,v):"",f.push(h[5]?a+p:p+a)}}return f.join("")},t.cache={},t.parse=function(e){var t=e,n=[],r=[],i=0;while(t){if((n=/^[^\x25]+/.exec(t))!==null)r.push(n[0]);else if((n=/^\x25{2}/.exec(t))!==null)r.push("%");else{if((n=/^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(t))===null)throw"[sprintf] huh?";if(n[2]){i|=1;var s=[],o=n[2],u=[];if((u=/^([a-z_][a-z_\d]*)/i.exec(o))===null)throw"[sprintf] huh?";s.push(u[1]);while((o=o.substring(u[0].length))!=="")if((u=/^\.([a-z_][a-z_\d]*)/i.exec(o))!==null)s.push(u[1]);else{if((u=/^\[(\d+)\]/.exec(o))===null)throw"[sprintf] huh?";s.push(u[1])}n[2]=s}else i|=2;if(i===3)throw"[sprintf] mixing positional and named placeholders is not (yet) supported";r.push(n)}t=t.substring(n[0].length)}return r};var n=function(e,n,r){return r=n.slice(0),r.splice(0,0,e),t.apply(null,r)};e.sprintf=t,e.vsprintf=n})(typeof exports!="undefined"?exports:window);
/*! Author: GI <gi1242+js@nospam.com> (replace nospam with gmail) * pTooltip */
(function($){function closeTip(tip,uid){var d=tip.data("pTooltip");if(uid===d.uid&&!d.mouseEntered)tip.hide()}$.fn.pTooltip=function(args){var options={tipCloseDelay:500,findToolTip:function(t){return $("#"+t.attr("title"))},tipPosition:function(t){return{my:"center top+15",at:"center bottom",of:t,collision:"fit flip"}}};$.extend(options,args);return this.each(function(){var t=$(this);var tip=options.findToolTip(t);if(tip.length===0)throw"No element with ID "+t.attr("title");t.removeAttr("title");t.mouseover(function(){$(":visible:data(pTooltip)").hide();var d=$.extend(tip.data("pTooltip"),{mouseEntered:false,uid:0});tip.data("pTooltip",d);tip.show().position(options.tipPosition(t))});t.mouseleave(function(){var uid=$.now();var d=$.extend(tip.data("pTooltip"),{uid:uid});tip.data("pTooltip",d);setTimeout(function(){closeTip(tip,uid)},options.tipCloseDelay)});tip.mouseover(function(){var d=$.extend(tip.data("pTooltip"),{mouseEntered:true});tip.data("pTooltip",d)});tip.mouseleave(function(){tip.hide()});tip.hide().addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content")})}})(jQuery);


/*! jQuery localtime plugin * Copyright (c) 2011-2013 Greg Thomas */
(function($){$.localtime=function(){var formatList={localtime:"yyyy-MM-dd HH:mm:ss"};var longMonths=["January","February","March","April","May","June","July","August","September","October","November","December"];var ordinals=["th","st","nd","rd"];var amPmHour=function(hour){return hour>=13?hour-12:hour==="0"?12:hour};var formatLocalDateTime=function(objDate,timeFormat){var year=objDate.getFullYear().toString();var month=(objDate.getMonth()+1).toString();var date=objDate.getDate().toString();var hour=objDate.getHours().toString();var minute=objDate.getMinutes().toString();var second=objDate.getSeconds().toString();var millisecond=objDate.getMilliseconds().toString();var tzOffset=objDate.getTimezoneOffset();var tzSign=tzOffset>0?"-":"+";tzOffset=Math.abs(tzOffset);if(timeFormat===undefined){var cssClass;for(cssClass in formatList)if(formatList.hasOwnProperty(cssClass)){timeFormat=formatList[cssClass];break}if(timeFormat===undefined)return objDate.toString()}var formattedDate="",pattern="";for(var i=0;i<timeFormat.length;i++){pattern+=timeFormat.charAt(i);if(pattern==="'"){i++;for(;i<timeFormat.length;i++){var literalChar=timeFormat.charAt(i);if(literalChar==="'"){pattern="";break}formattedDate+=literalChar}}else if(pattern==="\\"&&(i<timeFormat.length-1&&timeFormat.charAt(i+1)==="'")){i++;formattedDate+="'";pattern=""}else if(i===timeFormat.length-1||timeFormat.charAt(i)!==timeFormat.charAt(i+1)){switch(pattern){case "d":formattedDate+=date;break;case "dd":formattedDate+=("0"+date).slice(-2);break;case "M":formattedDate+=month;break;case "MM":formattedDate+=("0"+month).slice(-2);break;case "MMM":formattedDate+=longMonths[month-1].substr(0,3);break;case "MMMMM":formattedDate+=longMonths[month-1];break;case "yy":formattedDate+=year.slice(-2);break;case "yyyy":formattedDate+=year;break;case "H":formattedDate+=hour;break;case "HH":formattedDate+=("0"+hour).slice(-2);break;case "h":formattedDate+=amPmHour(hour);break;case "hh":formattedDate+=("0"+amPmHour(hour)).slice(-2);break;case "m":formattedDate+=minute;break;case "mm":formattedDate+=("0"+minute).slice(-2);break;case "s":formattedDate+=second;break;case "ss":formattedDate+=("0"+second).slice(-2);break;case "S":formattedDate+=millisecond;break;case "SS":formattedDate+=("0"+millisecond).slice(-2);break;case "SSS":formattedDate+=("00"+millisecond).slice(-3);break;case "o":switch(date){case "11":case "12":case "13":formattedDate+=ordinals[0];break;default:var ordinalIndex=date%10;if(ordinalIndex>3)ordinalIndex=0;formattedDate+=ordinals[ordinalIndex];break}break;case "a":case "tt":formattedDate+=hour>=12?"PM":"AM";break;case "t":formattedDate+=hour>=12?"P":"A";break;case "z":formattedDate+=tzSign+parseInt(tzOffset/60,10);break;case "zz":formattedDate+=tzSign+("0"+parseInt(tzOffset/60,10)).slice(-2);break;case "zzz":formattedDate+=tzSign+("0"+parseInt(tzOffset/60,10)).slice(-2)+":"+("0"+tzOffset%60).slice(-2);break;default:formattedDate+=pattern;break}pattern=""}}return formattedDate};return{setFormat:function(format){if(typeof format==="object")formatList=format;else formatList={localtime:format}},getFormat:function(){return formatList},parseISOTimeString:function(isoTimeString){isoTimeString=$.trim(isoTimeString.toString());var fields=/^(\d{4})-([01]\d)-([0-3]\d)[T| ]([0-2]\d):([0-5]\d)(?::([0-5]\d)(?:\.(\d{3}))?)?Z$/.exec(isoTimeString);if(fields){var year=parseInt(fields[1],10);var month=parseInt(fields[2],10)-1;var dayOfMonth=parseInt(fields[3],10);var hour=parseInt(fields[4],10);var minute=parseInt(fields[5],10);var second=fields[6]?parseInt(fields[6],10):0;var millisecond=fields[7]?parseInt(fields[7],10):0;var objDate=new Date(Date.UTC(year,month,dayOfMonth,hour,minute,second,millisecond));if(objDate.getUTCFullYear()!==year||(objDate.getUTCMonth()!==month||objDate.getUTCDate()!==dayOfMonth))throw new Error(fields[1]+"-"+fields[2]+"-"+fields[3]+" is not a valid date");if(objDate.getUTCHours()!==hour)throw new Error(fields[4]+":"+fields[5]+" is not a valid time");return objDate}else throw new Error(isoTimeString+" is not a supported date/time string");},toLocalTime:function(timeField,timeFormat){if(Object.prototype.toString.call(timeField)!=="[object Date]")timeField=$.localtime.parseISOTimeString(timeField);if(timeFormat==="")timeFormat=undefined;return formatLocalDateTime(timeField,timeFormat)},formatObject:function(object,format){if(object.is(":input"))object.val($.localtime.toLocalTime(object.val(),format));else object.text($.localtime.toLocalTime(object.text(),format))},formatPage:function(){var format;var localiseByClass=function(){$.localtime.formatObject($(this),format)};var formats=$.localtime.getFormat();var cssClass;for(cssClass in formats)if(formats.hasOwnProperty(cssClass)){format=formats[cssClass];$("."+cssClass).each(localiseByClass)}$("[data-localtime-format]").each(function(){$.localtime.formatObject($(this),$(this).attr("data-localtime-format"))})}}}()})(jQuery);jQuery(document).ready(function($){$.localtime.formatPage()});
$.localtime.setFormat({});
/*! tipsy, facebook style tooltips for jquery // version 1.0.0a // (c) 2008-2010 jason frame [jason@onehackoranother.com] // released under the MIT license */
(function($){function maybeCall(thing,ctx){return typeof thing=="function"?thing.call(ctx):thing}function isElementInDOM(ele){while(ele=ele.parentNode)if(ele==document)return true;return false}function Tipsy(element,options){this.$element=$(element);this.options=options;this.enabled=true;this.fixTitle()}Tipsy.prototype={show:function(){var title=this.getTitle();if(title&&this.enabled){var $tip=this.tip();$tip.find(".tipsy-inner")[this.options.html?"html":"text"](title);$tip[0].className="tipsy";$tip.remove().css({top:0,left:0,visibility:"hidden",display:"block"}).prependTo(document.body);var pos=$.extend({},this.$element.offset(),{width:this.$element[0].offsetWidth,height:this.$element[0].offsetHeight});var actualWidth=$tip[0].offsetWidth,actualHeight=$tip[0].offsetHeight,gravity=maybeCall(this.options.gravity,this.$element[0]);var tp;switch(gravity.charAt(0)){case "n":tp={top:pos.top+pos.height+this.options.offset,left:pos.left+pos.width/2-actualWidth/2};break;case "s":tp={top:pos.top-actualHeight-this.options.offset,left:pos.left+pos.width/2-actualWidth/2};break;case "e":tp={top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth-this.options.offset};break;case "w":tp={top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width+this.options.offset};break}if(gravity.length==2)if(gravity.charAt(1)=="w")tp.left=pos.left+pos.width/2-15;else tp.left=pos.left+pos.width/2-actualWidth+15;$tip.css(tp).addClass("tipsy-"+gravity);$tip.find(".tipsy-arrow")[0].className="tipsy-arrow tipsy-arrow-"+gravity.charAt(0);if(this.options.className)$tip.addClass(maybeCall(this.options.className,this.$element[0]));if(this.options.fade)$tip.stop().css({opacity:0,display:"block",visibility:"visible"}).animate({opacity:this.options.opacity});else $tip.css({visibility:"visible",opacity:this.options.opacity})}},hide:function(){if(this.options.fade)this.tip().stop().fadeOut(function(){$(this).remove()});else this.tip().remove()},fixTitle:function(){var $e=this.$element;if($e.attr("title")||typeof $e.attr("original-title")!="string")$e.attr("original-title",$e.attr("title")||"").removeAttr("title")},getTitle:function(){var title,$e=this.$element,o=this.options;this.fixTitle();var title,o=this.options;if(typeof o.title=="string")title=$e.attr(o.title=="title"?"original-title":o.title);else if(typeof o.title=="function")title=o.title.call($e[0]);title=(""+title).replace(/(^\s*|\s*$)/,"");return title||o.fallback},tip:function(){if(!this.$tip){this.$tip=$('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-inner"></div>');this.$tip.data("tipsy-pointee",this.$element[0])}return this.$tip},validate:function(){if(!this.$element[0].parentNode){this.hide();this.$element=null;this.options=null}},enable:function(){this.enabled=true},disable:function(){this.enabled=false},toggleEnabled:function(){this.enabled=!this.enabled}};$.fn.tipsy=function(options){if(options===true)return this.data("tipsy");else if(typeof options=="string"){var tipsy=this.data("tipsy");if(tipsy)tipsy[options]();return this}options=$.extend({},$.fn.tipsy.defaults,options);function get(ele){var tipsy=$.data(ele,"tipsy");if(!tipsy){tipsy=new Tipsy(ele,$.fn.tipsy.elementOptions(ele,options));$.data(ele,"tipsy",tipsy)}return tipsy}function enter(){var tipsy=get(this);tipsy.hoverState="in";if(options.delayIn==0)tipsy.show();else{tipsy.fixTitle();setTimeout(function(){if(tipsy.hoverState=="in")tipsy.show()},options.delayIn)}}function leave(){var tipsy=get(this);tipsy.hoverState="out";if(options.delayOut==0)tipsy.hide();else setTimeout(function(){if(tipsy.hoverState=="out")tipsy.hide()},options.delayOut)}if(!options.live)this.each(function(){get(this)});if(options.trigger!="manual"){var binder=options.live?"live":"bind",eventIn=options.trigger=="hover"?"mouseenter":"focus",eventOut=options.trigger=="hover"?"mouseleave":"blur";this[binder](eventIn,enter)[binder](eventOut,leave)}return this};$.fn.tipsy.defaults={className:null,delayIn:0,delayOut:0,fade:false,fallback:"",gravity:"n",html:false,live:false,offset:0,opacity:0.8,title:"title",trigger:"hover"};$.fn.tipsy.revalidate=function(){$(".tipsy").each(function(){var pointee=$.data(this,"tipsy-pointee");if(!pointee||!isElementInDOM(pointee))$(this).remove()})};$.fn.tipsy.elementOptions=function(ele,options){return $.metadata?$.extend({},options,$(ele).metadata()):options};$.fn.tipsy.autoNS=function(){return $(this).offset().top>$(document).scrollTop()+$(window).height()/2?"s":"n"};$.fn.tipsy.autoWE=function(){return $(this).offset().left>$(document).scrollLeft()+$(window).width()/2?"e":"w"};$.fn.tipsy.autoBounds=function(margin,prefer){return function(){var dir={ns:prefer[0],ew:prefer.length>1?prefer[1]:false},boundTop=$(document).scrollTop()+margin,boundLeft=$(document).scrollLeft()+margin,$this=$(this);if($this.offset().top<boundTop)dir.ns="n";if($this.offset().left<boundLeft)dir.ew="w";if($(window).width()+$(document).scrollLeft()-$this.offset().left<margin)dir.ew="e";if($(window).height()+$(document).scrollTop()-$this.offset().top<margin)dir.ns="s";return dir.ns+(dir.ew?dir.ew:"")}}})(jQuery);


// Google Analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-44043452-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = 'https://ssl.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

function trackButtonClick(e) {
    _gaq.push(['_trackEvent', e.target.id, 'clicked']);
};

// RePrioritize CSS Loading? 
/*
NativeCSSDoc = $('link[href*="tla4.min.css"]');
$('link[href*="tla4.min.css"]').remove();
$('head').append($(NativeCSSDoc));
*/
        
/** Don't run on frames or iframes, prevents google ads from loading the script **/
// if (window.top == window.self && document.location.href.match(/teamliquid.net/gim)){}

/*! Script-unique variables */
var scriptRoot = chrome.extension.getURL('/');
var scriptHTML = chrome.extension.getURL('/html/');
var scriptAssets = chrome.extension.getURL('/assets/');
var scriptImages = chrome.extension.getURL('/assets/images/');

/********** Read Settings **/
var optionStorage = null;

chrome.extension.sendMessage({
    message: "giveOptionStorage"
}, function (response) {
    optionStorage = response.optionStorage;
    continueOn();
});

function continueOn() {
    console.log('TeamLiquid Dark Theme settings loaded: ' + optionStorage);

    var lastReadMOTD = localStorage.getItem('tl_darktheme-lastreadmotd');
    var nightBegin = localStorage.getItem("darktheme_nightBegin");
    var nightEnd = localStorage.getItem("darktheme_nightEnd");

    /*! Announcement Revision Number */
    var AnnouncementNum = 8;
    
    /*! Constant variables */
    var RED = "#FF3333",
        GREEN = "#33FF33",
        BLUE = "#4FDFFF",
        SPACE = " ",
        DASH = "—",
        SDASH = "-",
        NEWLINE = "\n",
        BR = "<br>",
        BRBR = "<br><br>",
        HR = "<hr>",
        SEPARATOR = '<hr class="dark_theme_separator" />',
        NEWIMG = ' <img src="' + scriptImages + 'new.gif"> ',
        PATH = document.location.href,
        TITLE = document.title,
        loginToken;
    
        
        currentDate = new Date(),
        DarkThemeOptions = new Array(),
        settings = new Array();
        UserName = undefined;

    /*! WIP: Console Thing - Possible chat between users? || Possible IRC chat */
    //$.localtime.setFormat({});
    $('body').append('<div id="ms" style="position: fixed; top: 4px; right: 4px; color: #FFF; font-weight: bold;"></div>');
    //var ltime = $.localtime.toLocalTime( currentDate );
    //$('#ms').append('<span data-localtime-format="\'The time is\' HH:mm">'+currentDate+'</span>');
    //$('#ms').append('Hello, is there anyone there? <br /> <input type="text" style="background: transparent !important; color: #FFF !important; font-weight: bold; border: 0px !important; float: right; width: 100%; text-align: right; border-right: 1px solid #FFF;">');

    /*! Some nice Tooltips for native titles */
    $(".upcoming_text").tipsy({gravity: 'w'});
    $(".event.default").tipsy({gravity: 'w'});
    
    /********** jQuery-plugin: Style (c) Eagl3s1ght **/
    jQuery.fn.style = function (csstext) {
        $(this).attr('style', csstext);
    }
    jQuery.fn.src = function (imglink) {
        $(this).attr('src', imglink);
    }

    function Message(msg, time) {
        if (!time) var time = 50; //if time is not passed into function

        $('body').prepend('<div id="tldtMessage">' + msg + '</div>');
        if ($('#tldtMessage')) {
            setTimeout(function () {
                $('#tldtMessage').fadeOut('slow',

                function () {
                    $(this).remove();
                });
            }, 2000);
        }

        

    }

    function StaticMessage(msg) {
        msg = '<a href="#" id="staticMessage">' + msg + '</a>';
        $("body").children("table:nth-child(1)").prepend(msg);
    }

    //for the clockSettings
    function numbers(n) {
        var r = '';
        for (i = 0; i <= 23; i++) {
            r += ((i == n) ? '<option selected value="' + i + '">' : '<option value="' + i + '">') + ((i < 10) ? '0' + i : i) + '</option>'
        }
        return r;
    }

    /********** Set optionStorage if not set.. **/
    if (optionStorage == null) {
        localStorage.setItem('tl_darktheme-options', '-0000000000');
        optionStorage = localStorage.getItem('tl_darktheme-options');

        /********** If we have something to work with.. **/
    } else {

        /********** Check if optionStorage is corrupted (undefined) **/
        if (optionStorage.charAt(1) == "u" || optionStorage.search('undefined') > -1) {
            localStorage.setItem('tl_darktheme-options', '-0000000000');
            Message("Your optionStorage is corrupted and has been reset.");
        }

        for (i = 1; i <= 10; i++) {
            settings[i] = optionStorage.charAt(i);
        }
    }

    /********** When user uses DarkMode.. **/
    function __lightsOff() {

        
        /********** Create the Dimmer **/
        $('body').append('<div id="dimmer" style="display: none;"></div>');

        /********** Announcement **/
        function displayAnnouncement() {
            if ($('#dark_theme_message').html() == null) {

                // Create Dark Theme Message
                $("body").append('<div id="dark_theme_message"></div>');
                $("#dark_theme_message").append('<h1>Announcement #<span id="AnnouncementNum">' + AnnouncementNum + '</span> from TeamLiquid Dark Theme author Eagl3s1ght</h1>');
                $("#dark_theme_message").append('<a href="#" id="tldt_previous_announcement" title="View previous announcement">←</a><a href="#" id="tldt_next_announcement" title="View next announcement">→</a>');
                $("#dark_theme_message").append('<div id="dark_theme_message_container"></div>');
                $("#dark_theme_message_container").load(scriptHTML + 'Announcement-' + AnnouncementNum + '.html', function(){
                    // Replace new lines in grabbed doc with <br /> and submit changes to container
                    $("#dark_theme_message_container").html( $("#dark_theme_message_container").html().replace(/\n/g, '<br />') );
                    
                });
                
                if(AnnouncementNum >= 7){
                    $("#tldt_previous_announcement").show();
                }
                
                $("#dark_theme_message").append(''
                +'<div id="tldt_disclaimer"><strong>Disclaimer:</strong> This script manipulates the way teamliquid.net works - don\'t bother official staff about something the script has messed up. You can disable the extension on the <a href="chrome://extensions">Chrome extensions page.</a></div>'
                +'<div id="dark_theme_message_close">Click here to hide this notification until the next announcement is available</div>');
                
                var RequestedAnnouncement;
                
                // Next Announcement
                $("#tldt_next_announcement").on("click", function () {
                    console.log(RequestedAnnouncement);
                    if(RequestedAnnouncement+1 < AnnouncementNum+1){
                        RequestedAnnouncement = RequestedAnnouncement+1;
                        
                    
                        $("#dark_theme_message_container").load(scriptHTML + 'Announcement-' + RequestedAnnouncement + '.html', function(){
                            // Replace new lines in grabbed doc with <br /> and submit changes to container
                            $("#dark_theme_message_container").html( $("#dark_theme_message_container").html().replace(/\n/g, '<br />') );
                            
                            $("#AnnouncementNum").html(RequestedAnnouncement);
                            $("#tldt_previous_announcement").show();
                            
                            if(RequestedAnnouncement == AnnouncementNum){
                                $("#tldt_next_announcement").hide();
                            }
                        });
                    }
                });
                // Previous Announcement
                $("#tldt_previous_announcement").on("click", function () {
                    
                    console.log(RequestedAnnouncement);
                    if(AnnouncementNum-1 >= 7){
                        RequestedAnnouncement = AnnouncementNum-1;
                        
                        
                        $("#dark_theme_message_container").load(scriptHTML + 'Announcement-' + RequestedAnnouncement + '.html', function(){
                            // Replace new lines in grabbed doc with <br /> and submit changes to container
                            $("#dark_theme_message_container").html( $("#dark_theme_message_container").html().replace(/\n/g, '<br />') );
                            
                            $("#AnnouncementNum").html(RequestedAnnouncement);
                            $("#tldt_next_announcement").show();
                            
                            if(RequestedAnnouncement <= 7){
                                $("#tldt_previous_announcement").hide();
                                $("#tldt_next_announcement").show();
                            }
                        });
                    }
                });
                
                // Close Handler
                $("#dark_theme_message_close").on("click", function () {
                    $('#dark_theme_message').fadeOut('slow', function () {
                        $('#dark_theme_message').remove();
                    });
                    localStorage.setItem('tl_darktheme-lastreadmotd', AnnouncementNum);
                    Message("Hiding announcement, thanks for reading!");
                });
            }
        }

        if (lastReadMOTD != AnnouncementNum) {
            displayAnnouncement();
        }

        // Create Dark Theme Menu
        $("body").prepend($('<div class="dark_theme_menu"></div>').load(scriptHTML + 'DarkThemeMenu.html', function(){
               
            $(".dark_theme_menu>a,#dark_theme_menu_form_btn").tipsy({fade: true, gravity: 'n'});
            $('#dark_theme_menu_readannouncement').on("click", function(e){
                displayAnnouncement();
                trackButtonClick(e);
            }); 

        }));

        
        // So other scripts can recognize us
        $("head").attr('id', 'using-teamliquid-dark-theme'); 

        /*! Type: Add QuickQuote to posts | 
			Example: http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280&currentpage=4#75 */
        $('a[class*="submessage"][href*="/forum/postmessage.php?quote="]').each(function () {
            $(this).after('&nbsp;<a href="' + "http://www.teamliquid.net" + $(this).attr('href') + '" onclick="return false" class="quickquote">Quote++</a>');
        });

        /*! Hotkeys/Accesskeys for Input buttons */
        $('img[src*="/images/post/b.png"]').each(function () { $(this).parent().attr('accesskey', 'B');});
        $('img[src*="/images/post/i.png"]').each(function () { $(this).parent().attr('accesskey', 'I');});
        $('img[src*="/images/post/u.png"]').each(function () { $(this).parent().attr('accesskey', 'U');});
        $('img[src*="/images/post/link.png"]').each(function () { $(this).parent().attr('accesskey', 'L');});
        $('img[src*="/images/post/spoiler.png"]').each(function () { $(this).parent().attr('accesskey', 'A');});
        $('img[src*="/images/post/img.png"]').each(function () { $(this).parent().attr('accesskey', 'P');});
        $('img[src*="/images/post/quote.png"]').each(function () { $(this).parent().attr('accesskey', 'Q');});
        /* Hotkeys Explaination for Input buttons */
        $('form[action*="/forum/postmessage.php"]').append('<div id="tldt_hotkeys_informant"></div>');
        $('#tldt_hotkeys_informant').load(scriptHTML + 'HotkeysExplainer.html', function(){       });
        
        /*! ????????????????????? */
        $('body').find('img[src*="/mirror/layout/FP_Title.png"]').replaceWith('Starcraft Progaming News');
        $('body').find('img[src*="/images/frontpage/layout/ProgamingNews.png"]').replaceWith('Progaming News');

        $('body').find('img[src*="/images/frontpage/layout/"]').parent().css({
            'font-size': '12px',
            'line-height': '30px',
            'text-decoration': 'underline',
            'color': '#ccc'
        });
        
        $('img[src*="/images/frontpage/layout/"]').each(function () {
            $(this).replaceWith($(this).attr('alt'));
        });
        
        $('body').find('img[src*="/staff/R1CH/Rsm.png"]').attr('src', scriptImages + 'races/Rsm.png" class="race">');
        
        /********** setOptions **/
        /* if (settings[1] == "1") {
            $('body').append('<style>body{ background-image: none !important; background-color: #222222 !important; } </style>');
        } */

        /**********
				 Clock stuff
				 
				 http://jsfiddle.net/n9MSX/2/ 
				 http://jsfiddle.net/n9MSX/4/
				 http://jsfiddle.net/n9MSX/12/
				 http://jsfiddle.net/n9MSX/16/
				 
				**/

        if (settings[2] == "0") {

            function calcTime(city, offset) {

                // create Date object for current location
                d = new Date();

                // convert to msec
                // add local time zone offset
                // get UTC time in msec
                utc = d.getTime() + (d.getTimezoneOffset() * 60000);

                // create new Date object for different city
                // using supplied offset
                nd = new Date(utc + (3600000*offset));

                // return time as a string
                return nd; //"The local time in " + city + " is " + nd.toLocaleString();

            }
        
            /************************* Variable Helpers **/
            //br = ' <span class="dark_theme_menu_separator">-</span> ';
            br = ' <br /> ';
            ms = 1000 * 60 * 60;
            addZero = function (i) {
                return (i < 10) ? '0' + i : i;
            }
            addZeroBefore = function (i) {
                return (i < 10) ? '' + i + '0' : i;
            }

            /************************* Different timezones **/
            var timezoneName = [];
            var offset = [];
            
            timezoneName['Sydney'] = 'Sydney Time (GMT+11)';
            offset['Sydney'] = 11;

            timezoneName['KST'] = 'Korean Standard Time (GMT+9)';
            offset['KST'] = 9;

            timezoneName['CET'] = 'Central European Time (Sweden, Spain, Germany, France, GMT+1)';
            offset['CET'] = 1;

            timezoneName['GMT'] = 'Universal / Greenwich Time (England, GMT+0)';
            offset['GMT'] = 0 ;

            timezoneName['EST'] = 'Eastern Time (US, GMT-5)';
            offset['EST'] = -5;

            timezoneName['CST'] = 'Central Time (US, GMT-6)';
            offset['CST'] = -6;

            timezoneName['PST'] = 'Pacific Time (US, GMT-8)';
            offset['PST'] = -8;

            function updateTimeBar(){
                /************************* Initiate times **/
                var local_time = "" + addZero(currentDate.getHours()) + ":" + addZero(currentDate.getMinutes());
                //var local_time = $.localtime.toLocalTime("" + currentDate.getUTCFullYear() + "-" + currentDate.getUTCMonth() + "-" + currentDate.getUTCDay() + " " + currentDate.getUTCHours() + ":" + currentDate.getUTCMinutes() + "", "HH:mm");
                //$('#ms').append('<span data-localtime-format="\'The time is\' HH:mm">'+ltime+'</span>');

                /************************* Print times **/
                $("#timebar").html('<span title="Local">Local<span> ' + local_time + "</span></span>" + br +
                createDate( 'PST', offset['PST'] ) + br +
                createDate( 'CST', offset['CST'] ) + br +
                createDate( 'EST', offset['EST'] ) + br +
                createDate( 'GMT', offset['GMT'] ) + br +
                createDate( 'CET', offset['CET'] ) + br +
                createDate( 'KST', offset['KST'] ) + br +
                //createDate('Sydney', offset['Sydney'] ) + br +
                ' <a href="/forum/time.php">Time</a>');
            } updateTimeBar();
            
            setInterval( updateTimeBar, 60*1000);

            /************************* Function to create different times **/
            function createDate(timezone, offset) {
                var dateArray = [];
                dateArray['time'] = calcTime(timezone, offset);
                dateArray['h'] = calcTime(timezone, offset).getHours();
                dateArray['m'] = calcTime(timezone, offset).getMinutes();

                return '<span title="' + timezoneName[timezone] + '">' + timezone + ' <span>' + addZero(dateArray['h']) + ':' + addZero(dateArray['m']) + '</span>' + '</span>';
            }
        }

        // SC2 usericon sprites instead of SC1
        //if (settings[4] == "0") { 
        if (true) {
            $('body').append('<style>.usericon{background-image: url("' + scriptAssets + 'images/icon-sprite-tldt.png") !important; }</style>');
        }

        //-------------------------------- settings[5]Option **/
        if (settings[5]) {

            if (settings[5] == "1") {
                /*! Zerg 	*/
                $('body').find('img[src*="/staff/R1CH/Tsm.png"], img[src*="/tlpd/images/Ticon_small.png"]').attr('src', scriptAssets + 'images-races/Tsm.png');
                /*! Protoss */
                $('body').find('img[src*="/staff/R1CH/Psm.png"], img[src*="/tlpd/images/Picon_small.png"]').attr('src', scriptAssets + 'images-races/Psm.png');
                /*! Terran 	*/
                $('body').find('img[src*="/staff/R1CH/Zsm.png"], img[src*="/tlpd/images/Zicon_small.png"]').attr('src', scriptAssets + 'images-races/Zsm.png');

            }
        }

        if (settings[6] == "1") {
            $('.middle_content table').attr('width', '100%');
            $('.right_sidebar').attr('width', '180');

            var HDwidth = $('.middle_content').width();
            var HDheight = $('.middle_content').width() * 0.5625;

            $('object').css({
                'width': '',
                'height': ''
            });
            $('object').attr('width', HDwidth);
            $('object').attr('height', HDheight);


            $('table').each(function () {
                var _tempWidth = $(this).attr('width');
                var __tmpW = $(this).css('width');

                console.log(__tmpW);

                if (_tempWidth == '752' || _tempWidth == '742' || _tempWidth == '764' || _tempWidth == '732' || _tempWidth == '1136' || __tmpW == '752px' || __tmpW == '742px' || __tmpW == '764px' || __tmpW == '732px' || __tmpW == '1136px') {
                    $(this).removeAttr('width');
                    $(this).css({
                        'width': '100%'
                    });
                }

                $('#fp_wrap #news_main').css({
                    'margin': '0px auto'
                });
                $('#fp_leftwrap').css({
                    'width': '752px',
                    'margin': '0px auto'
                });
                $('#fp_leftwrap').append('<div style="clear: both;"></div>');

            });

            $('.middle_content').children('table').attr('width', '100%');
            $('.right_sidebar').attr('width', '180');
        }

        //-------------------------------- settings[7]Option
        if (settings[7] == "0") {
            $('body').append('<style>#streams_content{ line-height: 16px; } #streams_content img{ float: left; margin-right: 5px }</style>');
        }

        //-------------------------------- settings[8]Option
        if (settings[8] == "1") {
            $('object[type="application/x-shockwave-flash"]').css({
                'z-index': '100',
                    'position': 'relative'
            });
            $('object[type="application/x-shockwave-flash"]').append('<param name="wmode" value="transparent">');
        }

        //-------------------------------- settings[10]Option
        if (settings[10]) {

        }

    } // LightsOff()

    function getSelection() {
        var txt = '';
        if (window.getSelection) {
            txt = window.getSelection();
        } else if (document.getSelection) {
            txt = document.getSelection();
        } else if (document.selection) {
            txt = document.selection.createRange().text;
        } else return;

        return txt;
    }

    $(document).ready(function () {

        loginToken = ($('[href*="/mytlnet/logout.php?t="]').attr('href')).replace('/mytlnet/logout.php?t=', "");
        
        /********** Add structure to markup.. **/
        /*! Add class to sections left_sidebar, middle_content, right_sidebar */
        var temp = $('body > table > tbody > tr > td').children('table').eq(1).children('tbody').children('tr').children('td');
        $(temp).eq(0).addClass('left_sidebar');
        $(temp).eq(1).addClass('middle_content');
        $(temp).eq(2).addClass('right_sidebar');

        /*! Add class to post header, content, signature */
        $('td.titelbalk').parent().attr("class", "post_header");
        $('td.lichtb').parent().attr("class", "post_content");
        $('td.forumsig').parent().parent().parent().parent().parent().attr("class", "post_signature");

        /*! Remove br between posts */
        $('.post_header').parent().parent().siblings('br').remove();

        /*! Remove cellpadding from OP (first post in thread) */
        $('.post_header').parent('.solid').attr("cellpadding", "0");

        /********** When user wants Light Mode **/
        $('.dark_theme_menu_lightmode').on("click", function () {
            localStorage.setItem('tl_darktheme-enabled', '0');
            location.reload(true);
        });

        /********** When user wants Dark Mode **/
        $('#staticMessage').on("click", function () {

            if (localStorage.getItem('tl_darktheme-enabled') == '0') {
                localStorage.setItem('tl_darktheme-enabled', '1');
                $(this).slideUp(function () {
                    $(this).remove();
                });
                __lightsOff();
            } else if (localStorage.getItem('tl_darktheme-enabled') == '1') {
                __lightsOff();
                $(this).slideUp(function () {
                    $(this).remove();
                });
            }

        });

        /********** Thread page doesn't exist.. Go to last page **/
        if (/go to the last page in the thread./gim.test($('body').html())) {
            location.href = $('#contentcell').find('a[href*="&currentpage="]').attr('href');
        }

        /********** Get UserName **/
        UserMenu = $('div.loggedin').html();
        if (UserMenu != undefined) {
            UserMenu = UserMenu.split(' ');

            if (UserMenu[3] != undefined) {
                UserMenu = UserMenu[3].split('<br>');
                UserName = UserMenu[0];
            } else {
                UserName = 'Not Logged In';
            }
        }

        /********** Use Dark Theme @ night and Light Mode during daytime. **/
        if (localStorage.getItem('tl_darktheme-enabled') == '0') {
            StaticMessage('Teamliquid Dark Theme is disabled. Website may still not function properly. If you are having problems, disable the extension completely.');

        } else {
            if (settings[3] == "1") {
                if (nightBegin == null) localStorage.setItem("darktheme_nightBegin", 21)
                if (nightEnd == null) localStorage.setItem("darktheme_nightEnd", 7)

                var d_local = new Date();
                var hr = d_local.getHours();

                if (hr >= nightBegin || hr <= nightEnd) {
                    __lightsOff();
                    Message("- Lights OFF -");
                } else {
                    StaticMessage('Teamliquid Dark Theme is turned off (between ' + nightEnd + ' and ' + nightBegin + ' <sup>according to your settings</sup> <strong>You can temporarily enable Dark Theme by clicking here.</strong>).');
                    Message("- Lights ON -<br><br>Website may still not function properly, if you are having problems, disable the script in Greasemonkey script menu.");
                }

            } else {
                __lightsOff();
            }
        }

        /********** Lightbox when User clicks minimized images in Forum **/
        $('[onclick*="window.open(this.src)"]').each(function () {
            $(this)[0].onclick = null;
            $(this).attr('onclick', '');
            $(this).attr('onclick', null);

            var img = $(this).attr('src');

            $(this).on("click", function () {
                //create imagedimmer
                $('body').append('<div id="imageDimmer" style="height: 100%; width: 100%; position: fixed; left: 0px; top: 0px; z-index: 100 !important; background-color: black; opacity: 0.75; "></div>');
                //create popupimage
                $('body').append('<div id="popupImage_options" style="position: absolute; top: 5px; left: 5px; z-index: 102;">Dark Theme Feature!<br><small>Click on image/dimmer to close this lightbox.</small><br><a href="' + img + '">- Direct link to image</a><br><a href="http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280">- Report an error/suggest a feature</a></div>' + '<div id="popupImage" style="display: none;"><img src="' + img + '" id="popupImage_img">');
                //show popupimage
                $('#popupImage').attr('style', 'display: block; position: fixed; left: 50%; top: 50%; z-index: 101 !important; background: red; margin-left: -' + $('#popupImage_img')[0].naturalWidth / 2 + 'px; margin-top: -' + $('#popupImage_img')[0].naturalHeight / 2 + 'px;');
            });

            // remove popup and dimmer when user clicks one of them
            $('#imageDimmer, #popupImage').on("click", function () {
                $('#imageDimmer').remove();
                $('#popupImage').remove();
                $('#popupImage_options').remove();
            });
        });

        /********** If subforum = Website Feedback, display warning **/
        $('td[class*="topbar extramenu"]').each(function () {
            var _content = $(this).html();
            if (_content.indexOf('Website Feedback') > -1) {
                $(this).append('<div class="WebsiteFeedbackWarning">Note! You are using TeamLiquid Dark Theme which heavily manipulates the way this website works. Make sure you test the functionality of whatever might not be working with the script turned off before reporting a bug in this subforum!</div><style>.WebsiteFeedbackWarning {color: #FF5555; font-weight: bold; padding: 0px 10px; background: #000; -webkit-animation: color_change 1s infinite alternate;animation: color_change 1s infinite alternate;}@-webkit-keyframes color_change {from { color: red; }to { color: #CD5555; }}@keyframes color_change {from { color: red; }to { color: #CD5555; }}</style>');
            }
        });
        


        /********** Fix Menu Dropdown Positions **/
        var forum = $('#tl_navigation').children('li').eq(4);
        $(forum).children('ul').prepend('<li class="offset" style="width: 142px;"> </li>');
        var video = $('#tl_navigation').children('li').eq(8);
        $(video).children('ul').find('li.offset').attr('style', 'width: 291px');
        var features = $('#tl_navigation').children('li').eq(10);
        $(features).children('ul').find('li.offset').attr('style', 'width: 360px');


        /********** Feature: Dim stream-page **/
        $('.middle_content a[href*="/video/streams/"]').each(function () {
            if ($(this).html() == "View Stream List") {
                $(this).after('<a href="#dark_theme_dim" id="dark_theme_dim" style="display: block;	float: right;">Dim screen</a>');

                $('#dark_theme_dim').on("click", function () {
                    $('#dimmer').show();
                });
            }
        });

        /********** Remove dat ugly logout link **/
        $('a[href*="logout.php"]').html('<img src="' + scriptImages + 'application_exit.png" width="15" style="margin-bottom: -3px;" title="Sign out">');

        /*************************** Show Live Streamer Details on Hover */
        function addStreamListExtras() {

            $('.sidemenu a[href*="/video/streams/"]').not('[title="Show all live streams"], #nav_streams').each(function () {
                var html = $(this).html();
                var href = $(this).attr('href');
                var name = href.replace('/video/streams/', '');
                var title = $(this).attr('title');
                var viewers;
                if (title != undefined) viewers = title.match(/\d*?(?= viewers*?)/);

                if (settings[9] == "0") {            
                    $(this).mouseenter(function(e){
                        
                        // Hide title to prevent Browser anchor-tooltip
                        $(this).removeAttr('title');

                        e.preventDefault();
                        var overlayHeight = $('#tldt-streamer-overlay').height() / 2;
                        $('#tldt-streamer-overlay-streamer').html(name);
                        $('#tldt-streamer-overlay-viewers').html(viewers);

                        $('#tldt-streamer-overlay').show();
                        $('#tldt-streamer-overlay').css('left', event.pageX +8);
                        $('#tldt-streamer-overlay').css('top', event.pageY -overlayHeight);
                        
                    });
                    
                    $(this).attr('title', 'tldt-streamer-overlay');
                    $(this).pTooltip();
                }

            });

        }
        
        $('body').append('<div id="tldt-streamer-overlay"></div>');
        $("#tldt-streamer-overlay").load(scriptHTML + 'StreamerOverlay.html', function(){

            /*************************** Post clicked streamer to mystreams.php which adds favorite to database */
            $('.addFavorite').on("click", function () {
                name = decodeURI('' + $('#tldt-streamer-overlay-streamer').html() + '');
                console.log('Trying to add '+ name);
                
                $.post('/mytlnet/mystreams.php', {
                    useradd: '' + name + ''
                })
                .done(function () {
                    Message('Added ' + name + ' to favorite streamers!');
                    $(this).remove();

                    $('#streams_content').fadeOut();
                    $('#streams_content').load('http://www.teamliquid.net/ #streams_content', function () {
                        addStreamListExtras();
                        $('#streams_content').fadeIn();
                    });
                    
                })
                .fail(function () {
                    Message('Failed to perform action.');
                });
            });


            /*************************** Post clicked streamer to mystreams.php which removes favorite from database */
            $('.removeFavorite').on("click", function () {
                name = decodeURI('' + $('#tldt-streamer-overlay-streamer').html() + '');
                

                $.post('/mytlnet/mystreams.php', {
                    action: 'remove',
                    token: loginToken,
                    user: '' + name + ''
                })
                .done(function () {
                    Message('Removed ' + name + ' from favorite streamers!');
                    $(this).remove();

                    $('#streams_content').fadeOut();
                    $('#streams_content').load('http://www.teamliquid.net/ #streams_content', function () {
                        addStreamListExtras();
                        $('#streams_content').fadeIn();
                    });
                })
                .fail(function () {
                    Message('Failed to perform action.');
                });
            });
            

            // original function - refreshStreams()
            function refreshStreams2() {

                last_streams_refresh_time = $.now();
                $.post('/video/updateStreams', function (data) {
                    if (data.html) {
                        $('#streams_content').html(data.html);
                        addStreamListExtras();

                    }
                });
                $('#streams_refresh_icon').hide();


            }

            // original function - popOutPlayer()
            function popOutPlayer2(n) {
                    popoutwindow = window.open("http://www.teamliquid.net/video/streams/" + n + "/popout", "popout" + n, "height=488, width=810,toolbar=no,scrollbars=no,menubar=no,resizable=yes");
                    if (popoutwindow){
                        /*
                        popoutwindow.document.write (unescape("%3Chtml%3E%3Chead%3E%3Cstyle%20type%3D%27text%2Fcss%27%3Ebody%20%7Bmargin%3A0px%3Bpadding%3A0px%3Boverflow%3Ahidden%7D%3C%2Fstyle%3E%3Ctitle%3Esheevergaming%20-%20Team%20Liquid%20Live%20Stream%3C%2Ftitle%3E%3C%2Fhead%3E%3Cbody%3E%3Ciframe%20class%3D%22videoplayer%22%20src%3D%22http%3A%2F%2Fwww.twitch.tv%2Fembed%3Fchannel%3Dsheevergaming%22%20height%3D%22100%25%22%20width%3D%22100%25%22%20frameborder%3D%220%22%20scrolling%3D%22no%22%3E%3C%2Fiframe%3E%3C%2Fbody%3E%3C%2Fhtml%3E"));
                         */
                        if ($('#showVideoDiv').length == 0)	{
                            var showDiv = document.createElement("div");
                            var height = $('.videoplayer').height ();
                            $(showDiv).css ('width', $('.videoplayer').width ());
                            $(showDiv).css ('height', height);
                            $(showDiv).insertBefore ('.videoplayer');
                            $(showDiv).css ('background-color', '#ccc');
                            $(showDiv).attr ('id', 'showVideoDiv');

                            var htm = '<div style="height:' + ((height / 2) - 16) + 'px;"> </div><a style="display:block;text-align:center;margin-left:auto;margin-right:auto;" href="" onClick="return showVideo();">Show Video Player</a>';

                            try{
                                clonedElement = $('.videoplayer').clone ();
                            } catch (e){
                                //Work around jQuery bug in IE
                                htm = '<div style="height:' + ((height / 2) - 16) + 'px;"> </div><a style="display:block;text-align:center;margin-left:auto;margin-right:auto;" href="" onClick="window.location.reload(); return false;">Show Video Player</a>';
                            }
                            $(showDiv).html (htm);
                        }
                        $('.videoplayer').remove ();
                    }
                return false;
            }


            $('#streams_refresh_icon').on("click", function () {
                refreshStreams2();
            });

            
            /*************************** ^ clicked = open popOutPlayer2() */
            $('.openStreamWindow').on("click", function () {
                var StreamerName = $('#tldt-streamer-overlay-streamer').html();
                popOutPlayer2(StreamerName);
                
            });
            
            
            addStreamListExtras();        
        });


        /*! 
			QuickQuote-Shenanigans 
			http://jsfiddle.net/yQKg2/8/ 
		*/
        $('a[class*="quickquote"]').on("click", function () {
            var http = $(this).attr('href');

            $.get(http, function (data) {
                var quote = $(data).find('#reply_area').text();

                var html = $("#reply_area").val();
                html.replace(/^[\s\S]*?(?=\[QUOTE\])/, "");
                $("#reply_area").val(html + "" + quote);
            });
        });
        
        $('body').append('<a href="#" id="quick-quote-anchor">QuickQuote</a>');

        var Quote = new Object();
        $(".post_content .forumPost").mouseup(function (event) {
            event.stopPropagation();

            Quote.Selection = window.getSelection();
            Quote.SelectionString = Quote.Selection.toString();
            Quote.SelectionNode = Quote.Selection.anchorNode;
            Quote.Header = $(Quote.SelectionNode).parent().parent().parent().parent('table').parent('td').parent('.post_content').siblings('.post_header').children('.titelbalk:nth-child(1)').children('.forummsginfo');
            Quote.User = $(Quote.Header).html().split('&nbsp;')[2].replace(' ', "");
            Quote.Date = $(Quote.Header).html().split('&nbsp;')[3].split('.')[1].substring(1);
            Quote.FormData = $("#reply_area").val();

            if (this.className == "forumPost" || this.className == "forumpost") {
                if (Quote.Selection != "" && Quote.Selection != null && Quote.Selection != undefined) {
                    $('#quick-quote-anchor').attr("selection", Quote.SelectionString);
                    $('#quick-quote-anchor').show();
                    $('#quick-quote-anchor').css('left', event.pageX + 10);
                    $('#quick-quote-anchor').css('top', event.pageY + 10);
                } else {
                    $('#quick-quote-anchor').hide();
                }
            } else {
                $('#quick-quote-anchor').hide();
            }
        });

        
        $("#quick-quote-anchor").on("click", function (e) {
            title = '[B]On ' + Quote.Date + ' ' + Quote.User + ' wrote: [/B] ';
            $("#reply_area").val(Quote.FormData + ((Quote.FormData == "") ? "" : "\n\n") + '[QUOTE]' + title + Quote.Selection + '[/QUOTE]' + "\n");

            $('#quick-quote-anchor').fadeOut(50);
            
            //Scroll to #reply_area - Do we actually want this feature, it can be pretty annoying.
            $('html, body').animate({
                scrollTop: $("#reply_area").offset().top
            }, 200);
        
            Message('Quoted ' + user + ', message: "' + Quote.Selection + '"');
            e.preventDefault();
        });


    }); // $(document).ready(function(){
    
    
}

