var BR = "<br>";

//for the clockSettings
function numbers(n){
 var r = '';
 for(i=0; i<=23; i++){ 
  r += ((i==n) ? '<option selected value="'+i+'">' : '<option value="'+i+'">' ) + ((i<10) ? '0'+i : i) +'</option>' 
 }
 return r;
}

function newSetting(settingsHandler, description){
	return '<input type="checkbox" id="' + settingsHandler + '">' +
		   '<label for="' + settingsHandler + '">' + description + '</label>' +
		   '<div id="clear"></div>';
}

$(document).ready(function(){ 

	/********** Read Settings **/
	var optionStorage = localStorage.getItem('tl_darktheme-options');
	var lastReadMOTD = localStorage.getItem('tl_darktheme-lastreadmotd');
	var nightBegin = localStorage.getItem("darktheme_nightBegin");
	var nightEnd = localStorage.getItem("darktheme_nightEnd");
	settings = new Array();

	$('#OptionStorageText').html(optionStorage);

	/********** Set optionStorage if not set.. **/
	if( optionStorage == null ){
		localStorage.setItem('tl_darktheme-options', '-0000000000');
		optionStorage = localStorage.getItem('tl_darktheme-options');
		
	/********** If we have something to work with.. **/
	} else {

		/********** Check if optionStorage is corrupted (undefined) **/
		if(optionStorage.charAt(1) == "u" || optionStorage.search( 'undefined' ) > -1) {
			localStorage.setItem('tl_darktheme-options', '-0000000000');
			Message("Your optionStorage is corrupted and has been reset.");
		}

		settings[1]	 = optionStorage.charAt(1); 
		settings[2]	 = optionStorage.charAt(2); 
		settings[3]  = optionStorage.charAt(3); 
		settings[4]  = optionStorage.charAt(4); 
		settings[5]  = optionStorage.charAt(5); 
		settings[6]  = optionStorage.charAt(6); 
		settings[7]  = optionStorage.charAt(7); 
		settings[8]  = optionStorage.charAt(8); 
		settings[9]  = optionStorage.charAt(9); 
		settings[10] = optionStorage.charAt(10);
	}

	$("#optionStorageReset").live({ click: function() {
			optionStorage = '-0000000000';
			localStorage.setItem('tl_darktheme-options', optionStorage); 
			Message('Your optionStorage has been reset!');
			
			$('#OptionStorageText').html(optionStorage); 
		}
	});

	//if people change checkboxes, store new change in variable
	$('#settings1, #settings2, #settings3, #settings4, #settings5, #settings6, #settings7, #settings8, #settings9, #settings10').live('change', function(){
		var id = $(this).attr('id');
		
		for(var i = 1; i < 10; i++){
			if("settings" + i == id){ 	
				if(settings[i] == 0){	
					settings[i] = '1'; 
					
					if(i == 3)
						$('#clockSettings').slideDown();
				} else { 
					settings[i] = '0'; 
					
					if(i == 3)
						$('#clockSettings').slideUp();
				}
			}
		}

		optionStorage = '-'+settings[1]+settings[2]+settings[3]+settings[4]+settings[5]+settings[6]+settings[7]+settings[8]+settings[9]+settings[10];
		
		//setOptions
		localStorage.setItem('tl_darktheme-options', optionStorage);
		$('#OptionStorageText').html(optionStorage);

	});


	/********** Toggle the idea description for the idea -list **/
	$(".dark_theme_idea").children("p").hide();
	$(".dark_theme_idea").live({ 
		click: function() {
			$toggled = $(this).hasClass('toggled');
			if(!$toggled){
			
				$(this).addClass('toggled');
				$(this).children("p").fadeIn();
				
			} else if($toggled){
			
				$(this).removeClass('toggled');
				$(this).children("p").hide();
			}
		}
	});
	
	// Set/Clear Default Input Value
	$('.default-value').each(function() {

		   var default_value = this.value;

		   $(this).focus(function(){
				   if(this.value == default_value) {
						   this.value = '';
				   }
		   });

		   $(this).blur(function(){
				   if(this.value == '') {
						   this.value = default_value;
				   }
		   });

	});

						
	$('select[name="sundown"]').change( function(){ 
		nightBegin = $('select[name="sundown"] option:selected').val();
		localStorage.setItem('darktheme_nightBegin', nightBegin ); 
		console.log(nightBegin);

	} );
	
	$('select[name="sunset"]').change( function(){ 
		nightEnd = $('select[name="sunset"] option:selected').val(); 
		localStorage.setItem('darktheme_nightEnd', nightEnd ); 
		console.log(nightEnd);
	} );
						
	
	var SettingsHTML = ''
		+'<div style="float: left; width: 50%;">'
			+ newSetting('settings1', "Don't use wallpaper background.")
			+ newSetting('settings2', "Don't show all timezones.")
				+'<div id="timeSettings" class="extraSettings">'
				+"There's nothing here yet!" + BR
				+'</div>'
			
			+ newSetting('settings3', "Use Dark Theme @ night and Light Mode during daytime.")
				+'<div id="clockSettings" class="extraSettings">'
				+'Sundown @ <select name="sundown">'+ numbers( nightBegin ) +'</select>' + BR
				+'Sunset @ <select name="sunset">'+ numbers( nightEnd ) +'</select>'
				+'</div>'

			+ newSetting('settings4', "Use old Starcraft 1 Usericons (avatars).")
			+ newSetting('settings5', "Use old TL Race Icons (stream icons, etc.).")
		+'</div>'
		
		+'<div style="float: left; width: 50%;">'			
			+ newSetting('settings6', "Increase width of website to 100% of browser viewport (experimental)")
			+ newSetting('settings7', "Don't put streamers race infront of name.")
			+ newSetting('settings8', "Decrease performance of Streamed Video & put stream window behind TL preferences-boxes.")
		+'</div>';
	$('#dark_theme_optionpane form').append(SettingsHTML);
	
	/********** Import Settings **/
	for(var i = 1; i < settings.length; i++){
		if(settings[i] == "1"){
			$('#settings' + i + '').attr("checked", "true");  
			
			if(i == 3){
				$('#clockSettings').show();
			}
		} else if(settings[i] == "0"){
			if(i == 2){
				$('#timeSettings').show();
			}
		}
	}
} );

/*
	DarkThemeOptions += ''
	+ SEPARATOR
	+'<div class="dark_theme_title">Feedback</div>'
	+'Post your idea or browse and vote on existing ideas!' + '<br>'
	+ SEPARATOR

	+'<div style="float: left; width: 52%;  border-right: 1px solid #222;">'
		+ '<div class="dark_theme_subtitle">Existing Ideas</div>'
		+ '<ul>' + getIdeas() + '</ul>'
	+'</div>'

	+'<div class="dark_theme_vseparator"></div>'

	+'<div style="float: left; width: 44%; margin-left: 14px;">'
		+'	<div class="dark_theme_subtitle">Add Your Idea</div>'
		
		+'	<input class="default-value" type="text" value="Enter your idea.." />'
		+'	<textarea class="default-value" style="width: 100%; height: 70px;">Describe your idea..</textarea>'
		+'	<input class="default-value" type="email" value="Your email address" />'
		+'	<input type="submit" value="Post Idea" />'
	+'</div>'

	+'<div class="clear"></div>'

	+ SEPARATOR
	+'</div>';
*/