var data = require("self").data;

// include script.
require("page-mod").PageMod({
  include: ["*.teamliquid.net"],
  
  // userscript, userstyle
  contentScriptWhen: 'end',
  contentScriptFile: data.url("assets/content.js"),
  contentStyleFile: data.url("assets/style.css")
});

// Construct a panel, loading its content from the "text-entry.html"
// file in the "data" directory, and loading the "get-text.js" script
// into it.
var toggle_options = require("panel").Panel({
  width: 642,
  height: 440,
  contentURL: data.url("options.html"),
  contentScriptFile: data.url("options.js")
});
 
// Create a widget, and attach the panel to it, so the panel is
// shown when the user clicks the widget.
require("widget").Widget({
  label: "Make Dark Theme act the way you want it to!",
  id: "toggle-options",
  contentURL: data.url("icon_16.png"),
  panel: toggle_options
});
