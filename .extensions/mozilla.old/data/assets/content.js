/*! Library scripts are compressed with:
 *  http://www.lotterypost.com/js-compress.aspx
 *  http://closure-compiler.appspot.com/home
 */
/********** Don't run on frames or iframes, prevents google ads from loading the script **/
if(window.top == window.self && document.location.href.match(/teamliquid.net/gim) ){

/*! Script-unique variables */
var scriptAssets = 'assets/';
var scriptHost   = 'assets/images/';

/********** Read Settings **/
var optionStorage = localStorage.getItem('tl_darktheme-options');
var lastReadMOTD = localStorage.getItem('tl_darktheme-lastreadmotd');
var nightBegin = localStorage.getItem("darktheme_nightBegin");
var nightEnd = localStorage.getItem("darktheme_nightEnd");

/*! Constant variables */
var RED    				= "#FF3333",
	GREEN  				= "#33FF33",
	BLUE   				= "#4FDFFF",
	SPACE  				= " ",
	DASH   				= "—",
	SDASH  				= "-",
	NEWLINE 			= "\n",
	BR 					= "<br>",
	BRBR 				= "<br><br>",
	HR 					= "<hr>",
	SEPARATOR 			= '<hr class="dark_theme_separator" />',
	NEWIMG 				= ' <img src="' + scriptHost + 'new.gif"> ',
	PATH 				= document.location.href,
	TITLE 				= document.title,
	
	currentDate 		= new Date(),
	DarkThemeOptions 	= new Array(),
	settings	 		= new Array();


	
/*! "Message of the Day" */
var MOTD_num    = 4;
var MOTD_msg 	= new Array();
	MOTD_msg[4] = ''				
				+ '<div style="margin: 8px 55px; ">'
					+ NEWIMG + "<strong>New in 18.7</strong>: " + BR
					+ BR + "- Lightmode-toggle has returned."
					+ BR + "- Stream list now shows number of viewers and plusses and minuses to add/remove from favorite streamers."
					+ BR + "- Stream list has direct links to popup-Player for the clicked stream, aswell as # of viewers for each stream."
					+ BR + "- You can now set your own settings for the third setting (<span style=\"text-decoration: underline; font-style: italic;\">Use Dark Theme @ night and Light Mode during daytime.</span>)!"

					+ BRBR 
					
					+ NEWIMG + "<strong>New in 18.5</strong>: " 
					+ BRBR + "- Minimized screenshots that you open in threads are no longer opened in a new tab- you now get up a \"lightbox\" with the picture."
					+ BR + "- The setting <span style=\"text-decoration: underline; font-style: italic;\">Use Dark Theme @ night and Light Mode during daytime.</span> is finally functional. If you enable it, the theme will be activated between 9PM - 7AM local time."
					
					
					+ BRBR + "If you have any inputs on the latest changes, feel free to post them in <a href=\"http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280\">the thread</a> or <a href=\"http://www.teamliquid.net/mytlnet/index.php?view=new&to=Eagl3s1ght\">privately by PM</a>!"				 
					+ BRBR + "<small style=\"color: #FFCC33;\"><strong>Disclaimer:</strong><br> Know that this script manipulates the way teamliquid.net works so make sure not to bother official staff about something the script has messed up! Thanks!</small>"
				+ '</div>';

/********** jQuery-plugin: Style (c) Eagl3s1ght **/
jQuery.fn.style = function(csstext) { 	$(this).attr('style', csstext); }
jQuery.fn.src = function(imglink) {		$(this).attr('src', imglink); 	}

function Message(msg,time){	
	if(!time) var time = 50; //if time is not passed into function

	if( $('#tldtMessage') ){
		$('#tldtMessage').fadeOut(time, 
			function() { 
			$(this).remove(); 
			}
		);
	}
	
	$('body').prepend('<div id="tldtMessage" style="position: fixed; width: 600px; left: 50%; color: #FFF; font: 25px Tahoma; margin-left: -300px; background: rgba(0,0,0,0.7); top: 80px; z-index: 10000;  text-align: center; border: 2px solid rgba(255,255,255,0.7); border-radius: 20px; padding: 25px;">'+msg+'</div>');
	setTimeout(function() {
		$('#tldtMessage').fadeOut('slow', 
			function() { 
			$(this).remove(); 
			}
		);
	}, 2000);
}

function StaticMessage(msg){
	msg = '<a href="#" id="staticMessage">' + msg + '</a>';
	$("body").children("table:nth-child(1)").prepend(msg);
}

function newSetting(settingsHandler, description){
	return '<input type="checkbox" id="' + settingsHandler + '">' +
		   '<label for="' + settingsHandler + '">' + description + '</label>' +
		   '<div id="clear"></div>';
}

//for the clockSettings
function numbers(n){
 var r = '';
 for(i=0; i<=23; i++){ 
  r += ((i==n) ? '<option selected value="'+i+'">' : '<option value="'+i+'">' ) + ((i<10) ? '0'+i : i) +'</option>' 
 }
 return r;
}
			
/********** jQuery-plugin: simpleTooltip (c) Eagl3s1ght **/
jQuery.fn.simpleTooltip = function() {

	if( $('#simpleTooltip').length == 0 ) 
		$('body').children("table:nth-child(1)").prepend('<div id="simpleTooltip"></div><style>a.event:hover, a.past_event:hover{ color: #FFF !important; background: #111 !important; }</style>');
	
	$('#simpleTooltip').css({
		'left' : 0,
		'top' : 0,
		'position' : 'absolute',
		'z-index' : 2,
		'font' : '12px Tahoma',
		'padding' : '4px 8px',
		'border' : '1px solid rgba(0,0,0,1)',
		'border-top-right-radius' : '5px',
		'border-bottom-radius' : '5px',
		'background' : 'rgba(0,0,0,0.8)',
		'color' : 'lightblue',
		'display' : 'block',
		'visibility' : 'hidden'
	});

	var $title = "";
	
    $(this).mouseenter(function(e){

        $title = $(this).attr('title');
		$(this).removeAttr('title');
	
		// create a list of all the events
		title_output = $title.replace(/-/gi, "<br>-");
		if ( title_output.indexOf('<br>') == 0 ) 
			title_output = title_output.substr(4);
		
		// append the list to our tooltip
		$('#simpleTooltip').html(title_output);

		
		var left_offset = 0; //4
		var position = $(this).position();
		
		$('#simpleTooltip').css({
			'left' : position.left + $(this).width() + left_offset,
			'top' : position.top,
			'visibility' : 'visible'
        });
    });
          
    $(this).mouseout(function(e){

		$(this).attr('title', $title);
		
		$('#simpleTooltip').css({ 
			'visibility' : 'hidden' 
		});
		
        $(this).attr('title', $title);
    });
	
	
};

	/********** Set optionStorage if not set.. **/
	if( optionStorage == null ){
		localStorage.setItem('tl_darktheme-options', '-0000000000');
		optionStorage = localStorage.getItem('tl_darktheme-options');
		
	/********** If we have something to work with.. **/
	} else {
	
		/********** Check if optionStorage is corrupted (undefined) **/
		if(optionStorage.charAt(1) == "u" || optionStorage.search( 'undefined' ) > -1) {
			localStorage.setItem('tl_darktheme-options', '-0000000000');
			Message("Your optionStorage is corrupted and has been reset.");
		}
	
		settings[1]	 = optionStorage.charAt(1); 
		settings[2]	 = optionStorage.charAt(2); 
		settings[3]  = optionStorage.charAt(3); 
		settings[4]  = optionStorage.charAt(4); 
		settings[5]  = optionStorage.charAt(5); 
		settings[6]  = optionStorage.charAt(6); 
		settings[7]  = optionStorage.charAt(7); 
		settings[8]  = optionStorage.charAt(8); 
		settings[9]  = optionStorage.charAt(9); 
		settings[10] = optionStorage.charAt(10);
	}

/********** When user uses DarkMode.. **/
function __lightsOff(){

	/********** Add Stylesheet **/
	$("head").append('<link rel="stylesheet" href="' + scriptAssets + 'style.css" type="text/css" id="using-teamliquid-dark-theme">');

	/*************************** Add favorite streamer by clicking the plus */
	function addStreamListExtras(){
		$('.right_sidebar a[href*="/video/streams/"]').not('[title="Show all live streams"], #nav_streams').each( 
			function(){
				var html = $(this).html();
				var href = $(this).attr('href');
				var name = href.replace('/video/streams/', '');
				var title = $(this).attr('title');
				var viewers; if(title != undefined) viewers = title.match(/\d*?(?= viewers*?)/);
			
				$(this).css({'position' : 'relative', 'height' : '50px'});
				
				$(this).hover( 
					function(e){ 	
						$(this).removeAttr('title');
						
						var streamerOverlayHTML = html
												+ '<div id="streamerOverlay" style="display: none; font-size: 10px; padding: 2px 5px; background: #000; border: 1px dotted #333; width: 120px; ">'
													+ '<span style="color: #FFFF99 !important;">' + viewers + ' viewers</span>'
													+ BR + '<a href="#" streamer="'+name+'" onclick="return false;" class="addFavorite" title="Add this streamer to favorites!" style="color: '+GREEN+' !important;">Add to Favorites</a>'
													+ BR + '<a href="#" onclick="return false;" streamer="'+name+'" class="removeFavorite" title="Remove this streamer from favorites!" style="color: '+RED+' !important;">Remove from Favorites</a>'
													+ BR + '<a href="#" streamer="'+name+'" onclick="return false;" class="openStreamWindow" style="color: #6699FF !important;">Pop-out Stream</a>'
												+ '</div>';
						
						
						$(this).html(streamerOverlayHTML);
						$('#streamerOverlay').slideDown();
					}, function () {
						$(this).attr('title', title);
						$(this).html(html);
						$(this).css({'height' : 'auto'});
					}
				);

			}
		);
	}
	addStreamListExtras();

	var DarkThemeMenu = ''
		+'<div class="dark_theme_menu">'
			+'<span class="dark_theme_menu_title">TeamLiquid Dark Theme</span>'
				+'<span class="dark_theme_menu_separator"> — </span> '
			+'<a class="dark_theme_menu_thread" href="http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280" title="Open the Official Dark Theme Thread showing the First page">Thread</a>'
				+' [ <a class="dark_theme_menu_thread" href="http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280&currentpage=9999" title="Open the Official Dark Theme Thread showing the Last page">Last page</a> ] '
				+'<span class="dark_theme_menu_separator"> — </span>'
			+'<a class="dark_theme_menu_lightmode" href="#" onclick="return false" title="Switch to Light Mode">Lightmode</a>'
				+' <span class="dark_theme_menu_separator"> — </span>'
			+'<a class="dark_theme_menu_readannouncement" href="#" onclick="return false" title="Read the latest announcement">Latest announcement</a>'
			+'</div>'
			+'<div id="dark_theme_menu_preload1" style="display: none; position: absolute; left: -9999px;"><img src="'+scriptHost+'darktheme_message-bottom.png"></div>'
			+'<div id="dark_theme_menu_preload2" style="display: none; position: absolute; left: -9999px;"><img src="'+scriptHost+'darktheme_message-bg.png">'
		+'</div>';
		
	/********** Append the TeamLiquid Dark Theme Menu **/
	$("body").children("table:nth-child(1)").prepend(DarkThemeMenu);

		/********** Change to DarkMode images **/
		$('img').each( function(){
			var src = $(this).attr('src');

			/*! Type: Input button styling */
				 if(src == '/images/post/b.png'			 	   	) $(this).attr('src', scriptHost + 'wysiwyg/b.png')
			else if(src == '/images/post/i.png'			   		) $(this).attr('src', scriptHost + 'wysiwyg/i.png')
			else if(src == '/images/post/u.png'			   		) $(this).attr('src', scriptHost + 'wysiwyg/u.png')
			else if(src == '/images/post/img.png'		   		) $(this).attr('src', scriptHost + 'wysiwyg/img.png')
			else if(src == '/images/post/link.png'		   		) $(this).attr('src', scriptHost + 'wysiwyg/link.png')
			else if(src == '/images/post/spoiler.png'	   		) $(this).attr('src', scriptHost + 'wysiwyg/spoiler.png')
			else if(src == '/images/post/quote.png'		   		) $(this).attr('src', scriptHost + 'wysiwyg/quote.png')
			else if(src == '/images/post/wikiize.png'	   		) $(this).attr('src', scriptHost + 'wysiwyg/wikiize.png')
			else if(src == '/images/post/tlpdize.png'	   		) $(this).attr('src', scriptHost + 'wysiwyg/tlpdize.png')
			else if(src == '/images/post/untlpdize.png'	   		) $(this).attr('src', scriptHost + 'wysiwyg/untlpdize.png')
			
			else if(src == '/mirror/smilies/smile.gif'	   		) $(this).attr('src', scriptHost + 'smilies/smile.gif') 	
			else if(src == '/mirror/smilies/clown.gif'	   		) $(this).attr('src', scriptHost + 'smilies/clown.gif')
			else if(src == '/mirror/smilies/confused.gif'  		) $(this).attr('src', scriptHost + 'smilies/confused.gif')
			else if(src == '/mirror/smilies/coool.gif'	   		) $(this).attr('src', scriptHost + 'smilies/coool.gif')	
			else if(src == '/mirror/smilies/devil.gif'	   		) $(this).attr('src', scriptHost + 'smilies/devil.gif')	 
            
			else if(src == '/images/usericons/lockdown.gif'		) $(this).attr('src', scriptHost + 'usericons/lockdown.gif')
			else if(src == '/images/usericons/happybirthday.gif') $(this).attr("src", scriptHost + "usericons/happybirthday.png"); 
		});

		/*! Type: Old Post */
		$('body').find('img[src*="/images/forum/oldpost.gif"]').attr('src', scriptHost + 'old_post.png'); 
		
		/*! Type: TLAF Logo Link */
		$('body').find('img[src*="/images/banners/TheLittleAppFactory.jpg"]').attr('src', scriptHost + 'TheLittleAppFactory.png'); 

		/*! Type: Add QuickQuote to posts | 
			Example: http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280&currentpage=4#75 */
		$('a[class*="submessage"][href*="/forum/postmessage.php?quote="]').each( function(){ 
			$(this).after('&nbsp;<a href="'+ "http://www.teamliquid.net" + $(this).attr('href') +'" onclick="return false" class="quickquote">Quote++</a>'); 
		} );
				 
		/*! Type: Featured Stream Stars | 
			Example: http://www.teamliquid.net/sc2/ */
		$('body').find('img[src*="/images/forum/star.gif"]').attr('src', scriptHost + 'star.png');

		/*! Type: box titlebar image swap | 
			Example: http://www.teamliquid.net/ */
		$('body').find('img[src*="/mirror/layout/CommunityNewsAndHeadlines.png"]').attr('src', scriptHost + 'CommunityNewsAndHeadlines.png'); 
		$('body').find('img[src*="/mirror/layout/SponsoredThreads.png"]').attr('src', scriptHost + 'SponsoredThreads.png'); 
		$('body').find('img[src*="/mirror/layout/Poll.png"]').attr('src', scriptHost + 'Poll.png'); 
		$('body').find('img[src*="/mirror/layout/Store.png"]').attr('src', scriptHost + 'Store.png'); 
		$('body').find('img[src*="/mirror/layout/FP_Spotlight.png"]').attr('src', scriptHost + 'FP_Spotlight.png'); 
		$('body').find('img[src*="/mirror/layout/FP_Title.png"]').replaceWith('Starcraft Progaming News');
		$('body').find('img[src*="/images/frontpage/layout/ProgamingNews.png"]').replaceWith('Progaming News');

		$('body').find('img[src*="/images/frontpage/layout/"]').parent().css({'font-size' : '12px', 'line-height' : '30px',	'text-decoration' : 'underline', 'color': '#ccc' });
		$('img[src*="/images/frontpage/layout/"]').each( function(){ $(this).replaceWith( $(this).attr('alt') ); } );
		
		
		/*! Type: Random */ 
		$('body').find('img[src*="/staff/R1CH/Rsm.png"]').attr('src', scriptHost + 'Rsm.png" class="race">'); 
			
		/********** setOptions **/
		if(settings[1] == "1"){
			$('body').append('<style>body{ background-image: none !important; background-color: #222 !important; } </style>');
		}

				/**********
				 Clock stuff
				 
				 http://jsfiddle.net/n9MSX/2/ 
				 http://jsfiddle.net/n9MSX/4/
				 http://jsfiddle.net/n9MSX/12/
				 http://jsfiddle.net/n9MSX/16/
				 
				**/
				
				function DST(){
					var today = new Date;
					var yr = today.getFullYear();
					var jan = new Date(yr,0);	// January 1
					var jul = new Date(yr,6);	// July 1
					// northern hemisphere test
					if (jan.getTimezoneOffset() > jul.getTimezoneOffset() && today.getTimezoneOffset() != jan.getTimezoneOffset()){
						return true;
						}
					// southern hemisphere test	
					if (jan.getTimezoneOffset() < jul.getTimezoneOffset() && today.getTimezoneOffset() != jul.getTimezoneOffset()){
						return true;
						}
					// if we reach this point, DST is not in effect on the client computer.	
					return false;
				}

				if(settings[2] == "0"){

					/************************* Variable Helpers **/
					br = ' <span class="dark_theme_menu_separator">-</span> ';
					ms = 1000 * 60 * 60;
					addZero = function(i){ return (i < 10) ? '0'+i : i; }

					/************************* Different timezones **/
					var offset = [];
					var timezoneName = [];
					
					offset['local'] = null;
					var DST = ( DST() == true ) ? 1 : 0; // Check if Daylight Savings is active

					timezoneName['Sydney'] = 'Sydney Time (GMT+11)';
					offset['Sydney'] = 11  + DST;
					
					timezoneName['KST'] = 'Korean Standard Time (GMT+9)';
					offset['KST'] = 9 + DST;
					
					timezoneName['CET'] = 'Central European Time (Sweden, Spain, Germany, France, GMT+1)';
					offset['CET'] = 1 + DST;
					
					timezoneName['GMT'] = 'Universal / Greenwich Time (England, GMT+0)';
					offset['GMT'] = 0 + DST;
					
					timezoneName['EST'] = 'Eastern Time (US, GMT-5)';
					offset['EST'] = -5  + DST;
					
					timezoneName['CST'] = 'Central Time (US, GMT-6)';
					offset['CST'] = -6  + DST;
					
					timezoneName['PST'] = 'Pacific Time (US, GMT-8)';
					offset['PST'] = -8  + DST;
					
					setInterval(function() {
					
						/************************* Initiate times **/
						d_local = new Date();
						off = d_local.getTimezoneOffset();
						local_timestamp = d_local.getTime();
						offset['local'] = d_local.getTimezoneOffset() / 60;

						// universal time
						universalTime = new Date();
						universalTime.setTime(local_timestamp + (offset['local'] * ms));
						uniTime = universalTime.getTime();
						utctime = local_timestamp + (offset['local'] * ms);

						/************************* Print times **/
						$("#timebar").html(''
										  + createDate('PST') + br
										  + createDate('CST') + br
										  + createDate('EST') + br
										  + createDate('GMT') + br
										  + createDate('CET') + br
										  + createDate('KST') + br
										  + createDate('Sydney') + br
										  + ' <a href="/forum/time.php">Time</a>');
					}, 1000);

					/************************* Function to create different times **/
					function createDate(timezone) {
						var dateArray = [];
						dateArray['time'] = new Date(utctime + (offset[timezone] * ms));
						dateArray['h'] = dateArray['time'].getHours();
						dateArray['m'] = dateArray['time'].getMinutes();
						dateArray['meridiem'] = (dateArray['h'] < 12) ? 'AM' : 'PM';

						return '<span title="' + timezoneName[timezone] + '">'
									+ timezone + ' <span style="color: #888;">' + addZero(dateArray['h']) + ':' + addZero(dateArray['m']) + '</span>'
							 + '</span>';
					}
				}

				if(settings[4] == "0"){
					for(var i = 0; i <= 10; i++){	$('img[src*="/images/usericons/"][src*="T' + i + '.gif"]').each(function(index) { $(this).attr("src", scriptHost + "usericons/T" + i + ".gif"); });		}
					for(var j = 0; j <= 10; j++){	$('img[src*="/images/usericons/"][src*="Z' + j + '.gif"]').each(function(index) { $(this).attr("src", scriptHost + "usericons/Z" + j + ".gif"); });		}
					for(var k = 0; k <= 10; k++){	$('img[src*="/images/usericons/"][src*="P' + k + '.gif"]').each(function(index) { $(this).attr("src", scriptHost + "usericons/P" + k + ".gif"); });		}
				}


				//-------------------------------- settings[5]Option **/
				if( settings[5] ){
					if(settings[5] == "1"){
						/*! Zerg 	*/ 
							$('body').find('img[src*="/staff/R1CH/Tsm.png"], img[src*="/tlpd/images/Ticon_small.png"]').attr('src', scriptHost + 'races/Tsm.png'); 
						/*! Protoss */ 
							$('body').find('img[src*="/staff/R1CH/Psm.png"], img[src*="/tlpd/images/Picon_small.png"]').attr('src', scriptHost + 'races/Psm.png'); 
						/*! Terran 	*/ 
							$('body').find('img[src*="/staff/R1CH/Zsm.png"], img[src*="/tlpd/images/Zicon_small.png"]').attr('src', scriptHost + 'races/Zsm.png'); 
					
					} else { //use new race icons
					
						/*! Zerg 	*/ 
							$('body').find('img[src*="/staff/R1CH/Zsm.png"], img[src*="/images/race/SC2Z10.png"], img[src*="/images/race/BWZ10.png"], img[src*="/tlpd/images/Zicon_small.png"]').attr('src', scriptHost + 'races/Zsm_new.png'); 
						/*! Protoss */ 
							$('body').find('img[src*="/staff/R1CH/Psm.png"], img[src*="/images/race/SC2P10.png"], img[src*="/images/race/BWP10.png"], img[src*="/tlpd/images/Picon_small.png"]').attr('src', scriptHost + 'races/Psm_new.png'); 						
						/*! Terran 	*/ 
							$('body').find('img[src*="/staff/R1CH/Tsm.png"], img[src*="/images/race/SC2T10.png"], img[src*="/images/race/BWT10.png"], img[src*="/tlpd/images/Ticon_small.png"]').attr('src', scriptHost + 'races/Tsm_new.png');
					}
				}

				if(settings[6] == "1"){
					$('.middle_content table').attr('width', '100%');
					$('.right_sidebar').attr('width', '180');
					
					var HDwidth = $('.middle_content').width();
					var HDheight = $('.middle_content').width() * 0.5625;
					
					$('object').css({'width' : '', 'height' : ''});
					$('object').attr('width', HDwidth );
					$('object').attr('height', HDheight );
					

					$('table').each( function(){		
						var _tempWidth = $(this).attr('width');
						var __tmpW = $(this).css('width');
						
						console.log(__tmpW);
						
						if(_tempWidth == '752' || _tempWidth == '742' || _tempWidth == '764' || _tempWidth == '732' || _tempWidth == '1136' ||
						   __tmpW == '752px' || __tmpW == '742px' || __tmpW == '764px' || __tmpW == '732px' || __tmpW == '1136px'){					
							$(this).removeAttr('width');
							$(this).css({'width' : '100%'});  
						}
						
						$('#fp_wrap #news_main').css({'margin' : '0px auto'});
						$('#fp_leftwrap').css({'width' : '752px', 'margin' : '0px auto'});
						$('#fp_leftwrap').append('<div style="clear: both;"></div>');
						
					});

					$('.middle_content').children('table').attr('width', '100%');
					$('.right_sidebar').attr('width', '180');
				}

				//-------------------------------- settings[7]Option
				if(settings[7] == "0"){
					$('body').append('<style>#streams_content{ line-height: 16px; } #streams_content img{ float: left; margin-right: 5px }</style>');
				}

				//-------------------------------- settings[8]Option
				if(settings[8] == "1"){
					$('object[type="application/x-shockwave-flash"]').css({
						'z-index' : '100', 
						'position' : 'relative'
					}); 
					$('object[type="application/x-shockwave-flash"]').append('<param name="wmode" value="transparent">');
				}


				//-------------------------------- settings[9]Option
				if( settings[9] ){

				}

				//-------------------------------- settings[10]Option
				if( settings[10] ){

				}
			
} // LightsOff()
	
function getSelection() {
	var txt = '';
	if (window.getSelection) {
		txt = window.getSelection();
	}
	else if (document.getSelection) {
		txt = document.getSelection();
	}
	else if (document.selection) {
		txt = document.selection.createRange().text;
	} else return;
		
	return txt;
}

$(document).ready(function(){ 

		/********** Add structure to markup.. **/
			/*! Add class to sections left_sidebar, middle_content, right_sidebar */ 
			var temp = $('body > table > tbody > tr > td').children('table').eq(1).children('tbody').children('tr').children('td');
			$(temp).eq(0).addClass('left_sidebar');
			$(temp).eq(1).addClass('middle_content');
			$(temp).eq(2).addClass('right_sidebar');
			
			/*! Add class to post header, content, signature */ 
			$('td.titelbalk').parent().attr("class", "post_header");
			$('td.lichtb').parent().attr("class", "post_content");
			$('td.forumsig').parent().parent().parent().parent().parent().attr("class", "post_signature");
			
			/*! Remove br between posts */
			$('.post_header').parent().parent().siblings('br').remove();

			/*! Remove cellpadding from OP (first post in thread) */ 
			$('.post_header').parent('.solid').attr("cellpadding", "0");
			
		/********** When user wants Light Mode **/
		$('.dark_theme_menu_lightmode').live("click", function(){
			localStorage.setItem('tl_darktheme-enabled', '0');
			location.reload(true);
		});	

		/********** When user wants Dark Mode **/
		$('#staticMessage').live("click", function(){
		
			if( localStorage.getItem('tl_darktheme-enabled') == '0' ){
				localStorage.setItem('tl_darktheme-enabled', '1');
				$(this).slideUp( function(){ $(this).remove(); } );
				__lightsOff();
			} else if ( localStorage.getItem('tl_darktheme-enabled') == '1' ) {
				__lightsOff(); 
				$(this).slideUp( function(){ $(this).remove(); } );
			}

		});
		
		/********** Thread page doesn't exist.. **/
		if ( /go to the last page in the thread./gim.test( $('body').html() ) ){
			location.href = $('.middle_content').find('a[href*="&currentpage="]').attr('href');
		}

		/********** Append the Dimmer **/
		$('body').append('<div id="dimmer" style="display: none;"></div>');	

		/********** Get UserName **/
		UserMenu = $('div.loggedin').html();
		UserMenu = UserMenu.split(' ');
		
		if(UserMenu[3] != undefined){
			UserMenu = UserMenu[3].split('<br>');
			UserName = UserMenu[0];
		} else {
			UserName = 'Not Logged In';
		}
	
		/********** Use Dark Theme @ night and Light Mode during daytime. **/
		if(localStorage.getItem('tl_darktheme-enabled') == '0'){
			StaticMessage('Teamliquid Dark Theme is disabled. Website may still not function properly. If you are having problems, disable the extension completely.');			

		} else {
			if(settings[3] == "1"){
				if( nightBegin == null ) localStorage.setItem("darktheme_nightBegin", 21)
				if(   nightEnd == null ) localStorage.setItem("darktheme_nightEnd", 7)
						
				var d_local = new Date();
				var hr = d_local.getHours();
				
				if(hr >= nightBegin || hr <= nightEnd){
					__lightsOff();
					Message("- Lights OFF -");
				} else {				
					StaticMessage('Teamliquid Dark Theme is turned off (between '+nightEnd+' and '+nightBegin+' <sup>according to your settings</sup> <strong>You can temporarily enable Dark Theme by clicking here.</strong>).');
					Message("- Lights ON -<br><br>Website may still not function properly, if you are having problems, disable the script in Greasemonkey script menu.");
				}
				
			} else {
				__lightsOff();
			}
		}
		
		/********** Lightbox when User clicks minimized images in Forum **/
		$('[onclick*="window.open(this.src)"]').each( function(){  			
			$(this)[0].onclick = null;
			$(this).attr('onclick', '');
			$(this).attr('onclick', null);

			var img = $(this).attr('src');
			
			$(this).click( function() {
					//create imagedimmer
					$('body').append('<div id="imageDimmer" style="height: 100%; width: 100%; position: fixed; left: 0px; top: 0px; z-index: 100 !important; background-color: black; opacity: 0.75; "></div>');
					//create popupimage
					$('body').append('<div id="popupImage_options" style="position: absolute; top: 5px; left: 5px; z-index: 102;">Dark Theme Feature!<br><small>Click on image/dimmer to close this lightbox.</small><br><a href="'+ img +'">- Direct link to image</a><br><a href="http://www.teamliquid.net/forum/viewmessage.php?topic_id=239280">- Report an error/suggest a feature</a></div>'
									+'<div id="popupImage" style="display: none;"><img src="'+ img +'" id="popupImage_img">');
					//show popupimage
					$('#popupImage').attr('style', 'display: block; position: fixed; left: 50%; top: 50%; z-index: 101 !important; background: red; margin-left: -'+$('#popupImage_img')[0].naturalWidth/2+'px; margin-top: -'+$('#popupImage_img')[0].naturalHeight/2+'px;');
			});  
			
			// remove popup and dimmer when user clicks one of them
			$('#imageDimmer, #popupImage').live("click", function(){
				$('#imageDimmer').remove();
				$('#popupImage').remove();
				$('#popupImage_options').remove();
			});
		});
		
		/********** If subforum = Website Feedback, display warning **/
		$('td[class*="topbar extramenu"]').each( function(){   
		var _content = $(this).html(); 
			if(_content.indexOf('Website Feedback') > -1){ 
				$(this).append('<div style="color: #FF5555; font-weight: bold; padding: 0px 10px; background: #000; ">Note! You are using TeamLiquid Dark Theme which heavily manipulates the way this website works. Make sure you test the functionality of whatever might not be working with the script turned off before reporting a bug in this subforum!</div>');   
			}   
		});

		/********** Fix Menu Dropdown Positions **/
		var forum = $('#tl_navigation').children('li').eq(4);
			$(forum).children('ul').prepend('<li class="offset" style="width: 142px;"> </li>');
		var video = $('#tl_navigation').children('li').eq(8);
			$(video).children('ul').find('li.offset').attr('style', 'width: 291px');
		var features = $('#tl_navigation').children('li').eq(10);
			$(features).children('ul').find('li.offset').attr('style', 'width: 360px');

		
		/********** Feature: Dim stream-page **/
		$('.middle_content a[href*="/video/streams/"]').each( function(){ 
			if( $(this).html() == "View Stream List" ){
				$(this).after('<a href="#dark_theme_dim" id="dark_theme_dim" style="display: block;	float: right;">Dim screen</a>'); 
				
				$('#dark_theme_dim').live({ click: function() {   
						$('#dimmer').show();
					} 
				});
			}
		});
		
		/********** Privacy Setting **/
		$('#privacyCheckbox').click( function(){
			if(checked){
				Message("let's do it!" + $(this).val() );
			} else {
				Message("cant :(" + $(this).val() );
			}
		} );

		/********** Remove dat ugly logout link **/
		_loggedin = $('div.loggedin').html();
		_loggedin = _loggedin.replace(/:/gim, '');

		$('div.loggedin').html(_loggedin);
		$('div.loggedin b').replaceWith('<img src="'+ scriptHost +'application_exit.png" width="15" style="margin-bottom: -3px;" title="Sign out">');

		/*************************** ^ clicked = open popOutPlayer2() */
		$('.openStreamWindow').live('click', function(){
			var n = $(this).attr('streamer');
			popOutPlayer2(n);
		} );
		
		
		/*************************** Post clicked streamer to mystreams.php which adds favorite to database */
		$('.addFavorite').live('click', 
		  function(){
			name = $(this).attr('streamer');

			$.post('/mytlnet/mystreams.php', {useradd: name}, function(data) {
				Message('Added '+name+' to favorite streamers!');
				
				$('#streams_content').load('http://www.teamliquid.net/ #streams_content', function() {
					addStreamListExtras();
				});
			}); 
		  }
		);

		
		/*************************** Post clicked streamer to mystreams.php which removes favorite from database */
		$('.removeFavorite').live('click', 
		  function(){
			name = $(this).attr('streamer');
			var loginToken = ( $('[href*="/mytlnet/logout.php?t="]').attr('href') ).replace('/mytlnet/logout.php?t=', "");
			
			$.post('/mytlnet/mystreams.php', {action: 'remove', token: loginToken, user: name}, function(data) {
				Message('Removed '+name+' from favorite streamers!');
				
				$('#streams_content').load('http://www.teamliquid.net/ #streams_content', function() {
				  addStreamListExtras();
				});
			}); 
		  }
		);
		
		
		// original function - refreshStreams()
		function refreshStreams2(){
			
			last_streams_refresh_time = new Date().getTime ();
			$.post ('/video/updateStreams', function(data) {
				if (data.html){
					$('#streams_content').html (data.html);
						
					addStreamListExtras(); 
					
				}
			});
			$('#streams_refresh_icon').hide();
			
			
		}
		
		// original function - popOutPlayer()
		function popOutPlayer2(n){

			popoutwindow = window.open("http://www.teamliquid.net/video/streams/" + n + "/popout", "popout" + n, "height=488, width=810,toolbar=no,scrollbars=no,menubar=no,resizable=yes");
			if (popoutwindow){
				if ($('#showVideoDiv').length == 0){
					var showDiv = document.createElement("div");
					var height = $('.videoplayer').height ();
					$(showDiv).css ('width', $('.videoplayer').width ());
					$(showDiv).css ('height', height);
					$(showDiv).insertBefore ('.videoplayer');
					$(showDiv).css ('background-color', '#ccc');
					$(showDiv).attr ('id', 'showVideoDiv');

					var htm = '<div style="height:' + ((height / 2) - 16) + 'px;"> </div><a style="display:block;text-align:center;margin-left:auto;margin-right:auto;" href="" onClick="return showVideo();">Show Video Player</a>';

					try{
						clonedElement = $('.videoplayer').clone ();
					} catch (e){
						//Work around jQuery bug in IE
						htm = '<div style="height:' + ((height / 2) - 16) + 'px;"> </div><a style="display:block;text-align:center;margin-left:auto;margin-right:auto;" href="" onClick="window.location.reload(); return false;">Show Video Player</a>';
					}
					$(showDiv).html(htm);
				}
				$('.videoplayer').remove();
			}
		}
		
		
		$('#streams_refresh_icon').live({ click: 
			function() {
				refreshStreams2();
			}
		});
		
		// Initiate simpleTooltip on calendar
		$('a.event, a.past_event').simpleTooltip();

		/********** Message Notification **/	
		if(lastReadMOTD != MOTD_num){ 
			displayAnnouncement( MOTD_msg );
		}	

		$('.dark_theme_menu_readannouncement').click(function(){
			displayAnnouncement( MOTD_msg );
		});
		
		function displayAnnouncement(MOTD_msg){
			if($('#dark_theme_message').html() == null){
				$("body").append(''
				+'<div id="dark_theme_message">'
				+'	<hr>'
				+'	<h1>Announcement #'+ MOTD_num +' from TeamLiquid Dark Theme author Eagl3s1ght</h1>'
				+'	<p>'+ MOTD_msg[MOTD_num] +'</p>'
				+'	<div id="dark_theme_message_close">Click here to hide this notification until the next announcement is available</div>'
				+'	<hr>'
				+'</div>');
			}
		}
		
		$("#dark_theme_message_close").live({ click: 
			function() {
				$('#dark_theme_message').fadeOut('slow', function() {
					$('#dark_theme_message').remove();
				});	
				localStorage.setItem('tl_darktheme-lastreadmotd', MOTD_num);
				Message("Hiding announcement, thanks for reading!");
			}
		});

			var d = new Date();
			var currentTime = Math.round(d.getTime() / 1000); // Unix time in seconds

				/********** Close Dimmer **/	
				$("#dimmer").live({ click: function() {
						$("#dark_theme_optionpane").hide();
						$("#dimmer").hide();
					}
				});

			$('a[class*="quickquote"]').click(function(){
				var http = $(this).attr('href');

				$.get(http, function(data){	
					var quote = $(data).find('#reply_area').text();	
					
					var html = $("#reply_area").val();
					html.replace(/^[\s\S]*?(?=\[QUOTE\])/, "");
					$("#reply_area").val(html + "" +  quote);
				});
			});

		/*! 
			QuickQuote-Shenanigans 
			http://jsfiddle.net/yQKg2/8/ 
		*/
			$('body').append('<a href="#" id="quick-quote-anchor">QuickQuote</a>');
			
			
			$(".post_content .forumPost").mouseup(function(event) {
				event.stopPropagation();
			
				var selection = getSelection();
				var selectionString = getSelection().toString();
				var node = selection.anchorNode;
				
				if (this.className == "forumPost" || this.className == "forumpost") {
					if (selection != "" && selection != null && selection != undefined){
						$('#quick-quote-anchor').show();
						$('#quick-quote-anchor').css('left', event.pageX + 10);
						$('#quick-quote-anchor').css('top', event.pageY + 10);
						
						$('#quick-quote-anchor').html('QuickQuote: ' + selection);
					} else {
						$('#quick-quote-anchor').hide();
					}
				} else {
					$('#quick-quote-anchor').hide();
				}
			});
			
			$("#quick-quote-anchor").click(function(e) {
				var quoteHeader = $(this).parent('tr').parent().parent('table').parent('td').parent('.post_content').siblings('.post_header').children('.titelbalk[0]').children('.forummsginfo').html();
				
				var node = selection.anchorNode;
				var postHeaderSpan = $(node).parent().parent().parent().parent().parent().parent().parent().children('.post_header').children('td').children('span').html();

				$(quoteHeader).find('img').remove();
				html = $("#reply_area").val();
				
				var splittedheader = postHeaderSpan.split('&nbsp;');
				var splittedheader2 = splittedheader[3].split('.');
				date = splittedheader2[1];
				user = splittedheader[2];
				
				title = '[B]On ' + date + ' ' + user + ' wrote: [/B] ';
				$("#reply_area").val(html + "\n\n" + '[QUOTE]' + title + selection + '[/QUOTE]' + "\n\n");
				
				$('#quick-quote-anchor').fadeOut(50);
				//To add: scroll to #reply_from
				Message('Quoted ' + user + ', message: "' + selection + '"');
				e.preventDefault();
			});	


}); // $(document).ready(function(){ 
} // if(window.top == window.self)